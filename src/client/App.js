import React, { useState } from 'react'

import useApi from './hooks/useApi'
import Header from './components/Header/Header'

import ArticleRow from './components/ArticleRow/ArticleRow'
import FormController from './forms/FormController'

import FloatButton from './components/FloatButton/FloatButton'
import TabBar from './components/TabBar/TabBar'

import styles from './App.module.css'

const App = () => {
	return (
		<>
			<Header className={ styles['header'] } />
			<main className={ styles['main'] }>
				<small>Your data should be render here</small>
			</main>
		</>
	)
}

export default App
