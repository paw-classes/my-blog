import React from 'react'

import * as forms from './'

import styles from './Forms.module.css'

const FormController = (props) => {
	const { form, onClose } = props
	const Form = forms[form]
	return (
		<>
			{ Form && <div onClick={ () => { onClose && onClose() } } className={ styles['backdrop'] } /> }
			{ Form && (
				<div className={ styles['form-container'] }>
					<Form onSuccess={ onClose }/>
				</div>
			 ) }
		</>
	)
}

export default FormController
