import React, { useState, useEffect } from 'react'

import useCreateApi from '../hooks/useCreateApi'

import styles from './Forms.module.css'

const ArticleForm = (props) => {
	const { onSuccess } = props
	const [data, setData] = useState({ title: '', summary: '' })
	const [createOrUpdate, submitting, success, error] = useCreateApi('/api/articles', onSuccess)
	
	useEffect(() => {
		if (success) {
			onSuccess && onSuccess(true)
		}
	}, [success, onSuccess])

	const handleSubmit = (e) => {
		e.preventDefault()
		if (!submitting) {
			createOrUpdate(data)
		}
	}
	return (
		<div>
			<form onSubmit={ handleSubmit }>
				<div className={ styles['form-header'] }>
					<button type="button" onClick={ () => { onSuccess && onSuccess() } }><strong>Close</strong></button>
					<button primary="true" type="submit"><strong>Save</strong></button>
				</div>
				<div className={ styles['form-content'] }>
					{ error && <div className={ styles['form-error'] }><small>{ error.message }</small></div> }
					<label className={ styles['text-input'] }>
						<div className={ styles['text-label'] }>
							<strong>Title</strong>
						</div>
						<input name='title' placeholder='Add a title' value={ data.title } onChange={ (e) => {
							setData({
								...data,
								title: e.target.value
							})
						} }/>
					</label>
					<label className={ styles['text-input'] }>
						<div className={ styles['text-label'] }>
							<strong>Summary</strong>
						</div>
						<textarea name='summary' placeholder='Add a summary' value={ data.summary } onChange={ (e) => {
							setData({
								...data,
								summary: e.target.value
							})
						} }/>
					</label>
				</div>
			</form>
		</div>
	)
}

export default ArticleForm
