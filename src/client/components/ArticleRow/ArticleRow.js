import React from "react"
import Markdown from "react-markdown"

import styles from './ArticleRow.module.css'

const ArticleRow = (props) => {
	const { title, summary, created } = props
	return (
		<div>
			<h4 className={ styles['title'] }>{ title }</h4>
			<Markdown>{ summary }</Markdown>
			<div>
				<small>{ created }</small>
			</div>
		</div>
	)
}

export default ArticleRow
