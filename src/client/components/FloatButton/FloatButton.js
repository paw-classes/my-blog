import React from 'react'

import styles from './FloatButton.module.css'

const FloatButton = (props) => {
	const { className, ...otherProps } = props
	return (
		<div className={ `float-button ${ styles['root'] } ${ className || '' }` }>
			<button className={ styles['button'] } { ...otherProps }>+</button>
		</div>
	)
}

export default FloatButton
