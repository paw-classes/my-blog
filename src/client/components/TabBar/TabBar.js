import React, { useRef } from 'react'

import useInStuck from '../../hooks/useIsStuck'

import styles from './TabBar.module.css'

const TabBar = (props) => {
	const { className, stickThreshold } = props
	const sticker = useRef()
	const isStuck = useInStuck(sticker.current, 'top', stickThreshold)
	return (
		<>
			<div ref={ sticker } />
			<div className={ `${ styles['root'] } ${ className || '' } ${ isStuck ? 'is-stuck' : '' }` }>
				<div className={ styles['tabs'] }>
					<li>
						<div className={ `${ styles['tab'] } ${ styles['is-selected'] }` }>
							<strong>
								<small>Homepage</small>
							</strong>
						</div>
					</li>
				</div>
			</div>
		</>
	)
}

export default TabBar
