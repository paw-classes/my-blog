import React from 'react'
import logo from '../../assets/logo.svg'
import styles from './Header.module.css'

const Header = (props) => {
	const { className } = props
	return (
		<header className={ `${ styles['header'] } ${ className || '' }` }>
			<div className={ styles['header-top-bar'] }>
				<img className={ styles['logo'] } src={ logo } alt='logo' />
			</div>
			<div className={ styles['header-content'] }>
				<div className={ styles['title-content'] }>
					<h1 className={ styles['title'] }>My coding blog</h1>
					<small>This is a blog to test my coding skills.</small>
				</div>
			</div>
		</header>
	)
}

export default Header
