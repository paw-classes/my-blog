import { useState, useEffect } from 'react'

const useInStuck = (element, positionAttribute = 'top', threshold = 0) => {
	const [isStuck, setIsStuck] = useState(false)
	useEffect(() => {
		const checkIfStuck = () => {
			if (!element) { return false }
			const pos = element.getBoundingClientRect()[positionAttribute]
			const computedStyle = getComputedStyle(element)
			const stickyPoint = parseInt(computedStyle[positionAttribute], 10) || 0

			let isStuck = false
			
			if (positionAttribute === 'top') {
				isStuck = pos <= (stickyPoint + threshold)
			} else if (positionAttribute === 'bottom') {
				isStuck = pos + stickyPoint >= (window.innerHeight - threshold)
			}
			
			setIsStuck(isStuck)
		}
		window.addEventListener('scroll', checkIfStuck)
		window.addEventListener('resize', checkIfStuck)
		return () => {
			window.removeEventListener('scroll', checkIfStuck)
			window.removeEventListener('resize', checkIfStuck)
		};
	}, [element, positionAttribute, threshold])
	return isStuck
}

export default useInStuck
