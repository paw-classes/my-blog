import { useState, useEffect, useCallback } from 'react'

const useApi = (pathname, limit) => {
	const [data, setData] = useState(null)
	const [loading, setLoading] = useState(false)
	const [error, setError] = useState(null)

	const fetchData = useCallback(() => {
		setError(null)
		setLoading(true)
		const abortController = new AbortController()
		const url = `${ pathname }${ limit ? `?limit=${ limit }` : '' }`
		fetch(url, {
			signal: abortController.signal,
			headers: {
				'Accept': 'application/json'
			}
		})
			.then((r) => {
				if (r.ok) {
					return r.json()
				} else {
					setError({
						status: r.status,
						message: r.statusText
					})
				}
			})
			.then(r => {
				setData(r)
				setLoading(false)
			})
			.catch((e) => {
				console.log('error', e)
				setLoading(false)
				setError({
					status: 500,
					message: e.toString && e.toString()
				})
			})

		return abortController.abort
	}, [pathname, limit])

	useEffect(() => {
		const abort = fetchData()
		return () => {
			abort && abort()
		};
	}, [fetchData])
	
	return [data, loading, error, fetchData]
}

export default useApi