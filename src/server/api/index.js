const Router = require('koa-router')

const users = require('./users')
const session = require('./session')

const articles = require('./articles')

const api = new Router({
	prefix: '/api'
})
api.get('/', (ctx) => { ctx.body = { status: 'ok' } })

api.use(users.allowedMethods())
api.use(users.routes())
api.use(session.allowedMethods())
api.use(session.routes())

api.use(articles.allowedMethods())
api.use(articles.routes())

module.exports = api