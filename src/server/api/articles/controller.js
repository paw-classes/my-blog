const Article = require('./model')

class ArticlesController {

	/**
	 * Get all articles
	 * @param {ctx} Koa Context
	 */
	async find(ctx) {
		const { sort = '', limit } = ctx.searchParams || {}
		const [sort_field, sort_order] = sort.split(',').map(s => s.length ? s : undefined)

		ctx.body = await Article
			.find()
			.sort({
				created: 'desc',
				[sort_field]: sort_order
			})
			.limit(parseInt(limit, 10) || 0)
	}

	/**
	 * Find a article
	 * @param {ctx} Koa Context
	 */
	async findById(ctx) {
		try {
			const article = await Article
				.findById(ctx.params.id)
				.populate('author')
				.populate('comments')
			if (!article) {
				ctx.throw(404)
			}
			ctx.body = article
		} catch (err) {
			if (err.name === 'CastError' || err.name === 'NotFoundError') {
				ctx.throw(404)
			}
			ctx.throw(500)
		}
	}

	/**
	 * Add a article
	 * @param {ctx} Koa Context
	 */
	async add(ctx) {
		try {
			const article = await new Article(ctx.request.body).save()
			ctx.set('Location', `/articles/${ article._id }`)
			ctx.status = 201
			ctx.body = article
		} catch (err) {
			ctx.throw(422)
		}
	}

	/**
	 * Update a article
	 * @param {ctx} Koa Context
	 */
	async update(ctx) {
		try {
			const article = await Article.findByIdAndUpdate(
				ctx.params.id,
				ctx.request.body,
				{
					new: true,
					useFindAndModify: false
				}
			)
			if (!article) {
				ctx.throw(404)
			}
			// ctx.status = 204 - 204 is No Content
			ctx.body = article
		} catch (err) {
			console.log('err', err)
			if (err.name === 'CastError' || err.name === 'NotFoundError') {
				ctx.throw(404)
			}
			ctx.throw(500)
		}
	}

	/**
	 * Delete a article
	 * @param {ctx} Koa Context
	 */
	async delete(ctx) {
		try {
			const article = await Article.findByIdAndRemove(ctx.params.id)
			if (!article) {
				ctx.throw(404)
			}
			ctx.body = article
		} catch (err) {
			if (err.name === 'CastError' || err.name === 'NotFoundError') {
				ctx.throw(404)
			}
			ctx.throw(500)
		}
	}
}

module.exports = new ArticlesController()
