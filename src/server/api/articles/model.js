const mongoose = require('mongoose')

const { Schema } = mongoose

const articleSchema = new Schema({
	title: {
		type: String,
		required: true
	},
	summary: {
		type: String
	},
	updated: {
		type: Date,
		default: Date.now
	},
	created: {
		type: Date,
		default: Date.now
	},
	author: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	comments: [{
		type: Schema.Types.ObjectId,
		ref: 'Comment'
	}]
})

articleSchema.pre('save', function (next) {
	if (this.isNew) {
		this.created = Date.now
		this.updated = Date.now
	} else {
		this.updated = Date.now
	}
	next()
})

module.exports = mongoose.model('Article', articleSchema)