require('dotenv').config()

const Koa = require('koa')
const mongoose = require('mongoose')
const koaBody = require('koa-body')

const searchParams = require('./middleware/searchParams')
const session = require('./middleware/session')
const api = require('./api')

const {
	API_PORT = 4000,
	MONGO_DB_HOST,
	MONGO_BD_PORT,
	MONGO_DB_NAME
} = process.env

mongoose.connect(
	`mongodb://${ MONGO_DB_HOST }:${ MONGO_BD_PORT }/${ MONGO_DB_NAME }`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useCreateIndex: true,
		useFindAndModify: false
	}
)
mongoose.connection.on('error', console.error);

const app = new Koa()

app
	.use(searchParams())
	.use(session)
	.use(koaBody())
	.use(api.allowedMethods())
	.use(api.routes())

	.listen(API_PORT, () => {
		console.log('Server started on port', API_PORT)
	})
